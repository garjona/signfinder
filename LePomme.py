from flask import Flask, render_template, redirect, url_for, request
from flask_mail import Mail, Message

app = Flask(__name__)
mail = Mail(app)
app.config['MAIL_SERVER'] = 'smtp.gmail.com'
app.config['MAIL_PORT'] = 465
app.config['MAIL_USERNAME'] = 'gaag14@gmail.com'
app.config['MAIL_PASSWORD'] = 'zchfpmmlamarnwuh'
app.config['MAIL_USE_TLS'] = False
app.config['MAIL_USE_SSL'] = True
mail = Mail(app)


@app.route('/', methods=['GET','POST'])
def hello_world():

    return render_template('portada.html')


@app.route('/index')
def hello():
    return redirect(url_for('hello_world'))

@app.route('/contact', methods=['GET','POST'])
def index():
    #print("---")
    #print(request.form.get('name'))
    #print("---")
    if (request.method == 'POST'):
        msg = Message('Nuevo mensaje del formulario de contacto signfinder', sender='gabriel.arjona.14@sansano.usm.cl', recipients=['francisco.vasquez.14@sansano.usm.cl'])
        msg.body = "Llego un correo del formulario de contacto de la pagina signfinder.cl o signfinder.feriadesoftware.cl \nDescripcion: \nNombre: " + request.form.get('name') + "\n" + "Mail: " + request.form.get('email') + "\n" + "Telefono: " + request.form.get('telefono') + "\n" + "Mensaje: " + request.form.get('message')
        mail.send(msg)
        return "ok"
    else:
        return "error"

'''
@app.route('/contact', methods=['GET','POST'])
def index():
    sendFlag = 0
    if (request.method == 'POST'):
        sendFlag = 1
        msg = Message('Nuevo mensaje del formulario de contacto signfinder', sender='gabriel.arjona.14@sansano.usm.cl', recipients=['francisco.vasquez.14@sansano.usm.cl'])
        msg.body = "Llego un correo del formulario de contacto de la pagina signfinder.cl o signfinder.feriadesoftware.cl \nDescripcion: \nNombre: " + request.form['name'] + "\n" + "Mail: " + request.form['email'] + "\n" + "Telefono: " + request.form['telefono'] + "\n" + "Mensaje: " + request.form['message']
        mail.send(msg)
        return render_template('portada.html')
    else:
        if (sendFlag != 0):
            return "ok"
        else:
            return "error"
'''

@app.route('/integranteV')
def CVFrancisco():
    return render_template('CVFrancisco.html')


@app.route('/integranteA')
def CVJorge():
    return render_template('CVJorge.html')


@app.route('/integranteM')
def CVFelipe():
    return render_template('CVFelipe.html')


@app.route('/integranteG')
def CVGabriel():
    return render_template('CVGabriel.html')


@app.route('/integranteR')
def CVAndrea():
    return render_template('CVAndrea.html')


@app.errorhandler(404)
def page_not_found(e):
    return render_template('404error.html'), 404


if __name__ == '__main__':
    app.run(host = "0.0.0.0", port = 5000, debug=True)
